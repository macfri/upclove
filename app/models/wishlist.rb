class Wishlist < ActiveRecord::Base
  belongs_to :user
  belongs_to :friend

  has_many :wishlist_detail, dependent: :destroy

  accepts_nested_attributes_for :wishlist_detail

end
