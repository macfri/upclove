ActiveAdmin.register Category do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  #permit_params :list, :of, :attributes, :on, :model

filter :enabled
filter :name
filter :created_at
filter :updated_at


  permit_params :name, :slug, :position, :enabled

  #
  # or
  #
  #permit_params do
     #permitted = [:permitted, :attributes]
     #permitted << :other if resource.something?
     #permitted
  #end


end
