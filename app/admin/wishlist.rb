ActiveAdmin.register Wishlist do

 #menu false
 #has_many WishlistDetail
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

actions :all, except: [:new, :edit]

#actions :all

  filter :status
  

  index do
    selectable_column
    column :id
    column :user
    column :status
    column :created_at
    actions
    #actions :all, except: [:new, :edit]
  end


show do

  panel "Header" do

    table_for wishlist do
      column "user" do |x|
        x.user.first_name
      end

      column "status" do |x|
        x.status
      end
      column "created_at" do |x|
        x.created_at
      end
      column "friend" do |x|
        x.friend.first_name
      end
    end
end    


  panel "Detalle" do


    table_for wishlist.wishlist_detail do

      column "photo" do |appointment|
          image_tag('/'+appointment.product.photo_path, width: '75')
            #"<img src='" + appointment.product.photo_path + "' />"
      end

      column "name" do |appointment|
        appointment.product.title
      end
      column "created_at" do |appointment|
        appointment.product.created_at
      end
    end




  end
end



 form do |f|
    f.inputs 'Header' do
      f.input :id
      f.input :user
      f.input :friend
      f.input :status
    end
    f.inputs do
      f.has_many :wishlist_detail, heading: 'Detalle', allow_destroy: false, new_record: false do |a|
        a.input :product
      end
    end
  end
end
