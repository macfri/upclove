class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def index
  end

  def catalog

    user_id = session[:user_id]
    if user_id.nil?
      return redirect_to home_url
    end

  	slug = params[:slug]
  	@categories = Category.all()

  	if slug
  		category = Category.find_by slug: slug
	  	if not category
			 render :text => 'Not Found', :status => '404'
	  	else
			 @products = Product.where(category: category).all()
		end
	else
		@products = Product.all()
	end
  end

  def addproduct
    slug = params[:slug]
    product = Product.find_by slug: slug
    if product
      products_ids = session[:products_id]
      if products_ids
        data_ids = products_ids.to_s + ',' + product.id.to_s
      else
        data_ids = product.id.to_s
      end
      session[:products_id] = data_ids
    end
    redirect_to wishlist_url
  end

  def removeproduct
    slug = params[:slug]
    product = Product.find_by slug: slug
    if product
      products_ids = session[:products_id]
      if not products_ids.nil?

        items_ids = products_ids.split(',')
        items_ids.delete(product.id.to_s)
        session[:products_id] = items_ids.join(',')

        if items_ids.count() < 1
          session.delete('products_id')
        end
      end    
    end
    redirect_to wishlist_url
  end

  def wishlist
    user_id = session[:user_id]
    if user_id.nil?
      return redirect_to home_url
    end    
    products_ids = session[:products_id]
    if not products_ids
      return redirect_to catalog_url
    else
      products_ids = session[:products_id].split(',')
      @products = Product.where(id: products_ids)
    end
  end

  def addwishlist
    user_id = session[:user_id]
    if not user_id
      return redirect_to home_url
    end
    user = User.find(user_id)
    if not user
      return redirect_to home_url
    end
    products_ids = session[:products_id]
    if not products_ids
      return redirect_to catalog_url
    end
    products_ids = products_ids.split(',')
    @products = Product.where(id: products_ids)
    friend = Friend.new
    friend.save()
    wishlist = Wishlist.new
    wishlist.user = user
    wishlist.friend = friend
    wishlist.status = 'pending'
    wishlist.save()
    @products.each do |p|
      wishlist_detail = WishlistDetail.new
      wishlist_detail.wishlist = wishlist
      wishlist_detail.product = p
      wishlist_detail.save()
    end
    session['wishlist_id'] = wishlist.id
    redirect_to addfriend_url
  end

  def thanks
  end


  def mywishlist
    session.delete('user_id')
    session.delete('products_id')
    session.delete('wishlist_id')
    wishlist_id = params[:id].to_i
    @wishlist = Wishlist.find_by id: wishlist_id
    if not @wishlist
      render :text => 'Not Found', :status => '404'
    end

    session['mywishlist_id'] = wishlist_id

  end

  def addfriend
    user_id = session[:user_id]
    if user_id.nil?
      return redirect_to home_url
    end
    user = User.find(user_id)
    if not user
      return redirect_to home_url
    end
    wishlist_id = session['wishlist_id']
    if not wishlist_id
      return redirect_to home_url
    end
    wishlist = Wishlist.find(wishlist_id)
    if not wishlist
      return redirect_to home_url
    end

    @url = mywishlist_url(wishlist.id.to_s) + '?fbrefresh=' + rand(100000).to_s
    p 'mywishlist *****************************************************************'
    p @url
    p 'mywishlist *****************************************************************'
    @client_id = App::Application.config.facebook_api_key
  end

end
