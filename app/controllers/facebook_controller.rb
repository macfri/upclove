#require 'oauth2' 

class FacebookController < ApplicationController

    def index
        client_id = App::Application.config.facebook_api_key
        client_secret = App::Application.config.facebook_api_secret
        authorize_url = App::Application.config.facebook_authorize_url
        redirect_uri = request.original_url
        
        code = params[:code]

        if not code
            url = "#{authorize_url}?client_id=#{client_id}&scope=email&redirect_uri=#{redirect_uri}"
            redirect_to url
        else
            graph_url = App::Application.config.facebook_graph_url
            access_token_url = App::Application.config.facebook_access_token_url

            params = {:params =>{
                :client_id =>client_id,
                :client_secret => client_secret,
                :redirect_uri => redirect_uri,
                :code => code}}

            r = RestClient.get access_token_url, params
            access_token = r.to_s.split("&")[0].split('=')[1]
            expires = r.to_s.split("&")[1].split('=')[1]
            
            url = "#{graph_url}/v2.2/me?access_token=#{access_token}"
            r = RestClient.get url
            data = JSON.parse(r.to_s)
            user = User.find_by fb_id: data['id']

            if not user
                user = User.new
                user.first_name = data['first_name']
                user.last_name = data['last_name']
                user.email = data['email']
                user.fb_id = data['id']
            else
           
                mywishlist_id = session['mywishlist_id']

                if not mywishlist_id.nil?
                    wishlist = Wishlist.find_by id: mywishlist_id
                    if not wishlist.nil?
                        if wishlist.user != user
                            p 'different user'
                            friend = Friend.find_by id: wishlist.friend.id
                            friend.first_name = data['first_name']
                            friend.last_name = data['last_name']
                            friend.fb_id = data['id']

                            p 'try update friend'
                            begin
                                friend.save
                            rescue Exception => e
                                p e
                            end

                            wishlist.status = 'complete'
                            begin
                                wishlist.save
                            rescue Exception => e
                                p e
                            end

                        else
                            p 'same user'
                        end
                    else
                        p 'not exists wishlist'
                    end
                    session.delete('mywishlist_id')
                else
                    p 'not exists session'
                end
            end

            user.fb_oauth_token = access_token
            user.fb_expires = expires
            
            begin
                user.save   
                session[:user_id] = user.id

                redirect_to catalog_url

            rescue Exception => e
                p e
            end

        end
    end

end
