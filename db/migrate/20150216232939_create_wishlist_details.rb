class CreateWishlistDetails < ActiveRecord::Migration
  def change
    create_table :wishlist_details do |t|
      t.references :wishlist, index: true
      t.references :product, index: true

      t.timestamps
    end
  end
end
