class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :slug
      t.integer :position
      t.boolean :enabled
      t.text :description
      t.text :photo_path
      t.belongs_to :category, index: true
      t.timestamps
    end
  end
end
