class CreateWishlists < ActiveRecord::Migration
  def change
    create_table :wishlists do |t|
      t.references :user, index: true
      t.references :friend, index: true
      t.string :status
      t.timestamps
    end
  end
end
