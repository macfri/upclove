class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :fb_id
      t.string :fb_oauth_token
      t.integer :fb_expires
      t.timestamps
    end
    add_index :users, :fb_id, :unique => true
    add_index :users, :email
  end
end
