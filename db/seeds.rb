# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

Product.delete_all()
Category.delete_all()

cat = Category.new
cat.name = 'Relojes'
cat.slug = 'relojes'
cat.save()

Product.create(title: 'Reloj 1', slug: 'p1', category: cat, photo_path: 'products/relojes/1.jpg')
Product.create(title: 'Reloj 2', slug: 'p2', category: cat, photo_path: 'products/relojes/2.jpg')
Product.create(title: 'Reloj 3', slug: 'p3', category: cat, photo_path: 'products/relojes/3.jpg')
Product.create(title: 'Reloj 4', slug: 'p4', category: cat, photo_path: 'products/relojes/4.jpg')
Product.create(title: 'Reloj 5', slug: 'p5', category: cat, photo_path: 'products/relojes/5.jpg')
Product.create(title: 'Reloj 6', slug: 'p5', category: cat, photo_path: 'products/relojes/6.jpg')
Product.create(title: 'Reloj 7', slug: 'p5', category: cat, photo_path: 'products/relojes/7.jpg')
Product.create(title: 'Reloj 8', slug: 'p5', category: cat, photo_path: 'products/relojes/8.jpg')

cat = Category.new
cat.name = 'Perfumes'
cat.slug = 'perfumes'
cat.save()

Product.create(title: 'Perfume 1', slug: 'p6', category: cat, photo_path: 'products/perfumes/1.jpg')
Product.create(title: 'Perfume 2', slug: 'p7', category: cat, photo_path: 'products/perfumes/2.jpg')
Product.create(title: 'Perfume 3', slug: 'p8', category: cat, photo_path: 'products/perfumes/3.jpg')
Product.create(title: 'Perfume 4', slug: 'p9', category: cat, photo_path: 'products/perfumes/4.jpg')
Product.create(title: 'Perfume 5', slug: 'p10', category: cat, photo_path: 'products/perfumes/5.jpg')
Product.create(title: 'Perfume 6', slug: 'p11', category: cat, photo_path: 'products/perfumes/6.jpg')
Product.create(title: 'Perfume 7', slug: 'p12', category: cat, photo_path: 'products/perfumes/7.jpg')


cat = Category.new
cat.name = 'Colonias'
cat.slug = 'colonias'
cat.save()

Product.create(title: 'Colonia 1', slug: 'p13', category: cat, photo_path: 'products/colonias/1.jpg')
Product.create(title: 'Colonia 2', slug: 'p14', category: cat, photo_path: 'products/colonias/2.jpg')
Product.create(title: 'Colonia 3', slug: 'p15', category: cat, photo_path: 'products/colonias/3.jpg')
Product.create(title: 'Colonia 4', slug: 'p16', category: cat, photo_path: 'products/colonias/4.jpg')
Product.create(title: 'Colonia 5', slug: 'p17', category: cat, photo_path: 'products/colonias/5.jpg')
Product.create(title: 'Colonia 6', slug: 'p18', category: cat, photo_path: 'products/colonias/6.jpg')
Product.create(title: 'Colonia 7', slug: 'p19', category: cat, photo_path: 'products/colonias/7.jpg')
Product.create(title: 'Colonia 8', slug: 'p20', category: cat, photo_path: 'products/colonias/8.jpg')
Product.create(title: 'Colonia 9', slug: 'p21', category: cat, photo_path: 'products/colonias/9.jpg')


cat = Category.new
cat.name = 'Ropa'
cat.slug = 'ropa'
cat.save()

Product.create(title: 'Ropa 1', slug: 'p22', category: cat, photo_path: 'products/ropa/1.jpg')
Product.create(title: 'Ropa 2', slug: 'p23', category: cat, photo_path: 'products/ropa/2.jpg')
Product.create(title: 'Ropa 3', slug: 'p24', category: cat, photo_path: 'products/ropa/3.jpg')
Product.create(title: 'Ropa 4', slug: 'p25', category: cat, photo_path: 'products/ropa/4.jpg')
Product.create(title: 'Ropa 5', slug: 'p26', category: cat, photo_path: 'products/ropa/5.jpg')
Product.create(title: 'Ropa 6', slug: 'p27', category: cat, photo_path: 'products/ropa/6.jpg')
Product.create(title: 'Ropa 7', slug: 'p28', category: cat, photo_path: 'products/ropa/7.jpg')
Product.create(title: 'Ropa 8', slug: 'p29', category: cat, photo_path: 'products/ropa/8.jpg')
Product.create(title: 'Ropa 9', slug: 'p30', category: cat, photo_path: 'products/ropa/9.jpg')